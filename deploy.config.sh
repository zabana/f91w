echo "[log] - Merging branch to master"
git checkout master && git merge develop && git push origin master
echo "[log] - Merge completed"

echo "[log] - Compiling project to build folder ..."
npm run build
echo "[log] - Build process done"

echo "[log] - Deploying files to server"
rsync -avP build/ user@host:/path/to/project
echo "[log] - Deployment completed"
