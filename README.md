## F91-W

F91-W is home to my photography.
It's a React.js front-end for an API that I wrote in Flask.
All the pictures are uploaded on an S3 bucket and are pulled and served to the
front end by the API.

Also, as a note to myself: don't forget the config :smile:
