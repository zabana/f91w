import {
  FETCH_PHOTOS,
  FETCH_PHOTOS_SUCCESS,
  FETCH_PHOTOS_ERROR,
  LOAD_MORE_PHOTOS,
  LOAD_MORE_PHOTOS_SUCCESS,
  LOAD_MORE_PHOTOS_ERROR,
} from '../actions/types';

const initialState = {
  allPhotos: [],
  photosToDisplay: [],
  currentPage: 1,
  requestFailed: false,
  loadMorePhotosFailed: false,
  error: {
    status: '',
    message: '',
  },
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case FETCH_PHOTOS:
      return state;

    case FETCH_PHOTOS_SUCCESS:
      return {
        ...state,
        allPhotos: payload.allPhotos,
        photosToDisplay: payload.photosToDisplay
      };

    case FETCH_PHOTOS_ERROR:
      return {
        ...state,
        requestFailed: true,
        error: payload.error
      };

    case LOAD_MORE_PHOTOS:
      return {...state};

    case LOAD_MORE_PHOTOS_SUCCESS:
      return {
        ...state,
        currentPage: payload.currentPage,
        photosToDisplay: payload.photosToDisplay
      };

    case LOAD_MORE_PHOTOS_ERROR:
      return {
        ...state,
        loadMorePhotosFailed: true,
        error: payload.error
      };

    default:
      return state;
  }
}
