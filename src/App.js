import React, { Component } from 'react';
import Header from './components/Header';
import PhotoList from './components/PhotoListConnector';
import Footer from './components/Footer';
import { Provider } from 'react-redux';
import './App.sass';
import store from './store';

class App extends Component {

    render() {
        return (
          <Provider store={store}>
            <main>
              <Header/>
              <PhotoList />
              <Footer />
            </main>
          </Provider>
        );
    }
}

export default App;
