const config = {

  API_URL: process.env.REACT_APP_F91W_API_URL,
  S3_BUCKET_NAME: process.env.REACT_APP_F91W_S3_BUCKET_NAME
};

export default config;
