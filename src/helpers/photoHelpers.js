import config from '../config';

const sanitiseFileName = fileName => {
  const regex = new RegExp(/[_-]/, 'g');
  return fileName.split('.')[0].replace(regex, ' ');
};

export const createFileURL = fileObject => {
  const url = `https://${config.S3_BUCKET_NAME}.s3.amazonaws.com/`;
  fileObject.url = url + fileObject.name;
  fileObject.name = sanitiseFileName(fileObject.name);
  return fileObject;
};

export const getChunkOfPhotosToDisplay = (photos, startIndex, pageNumber, amount) => {
  return photos.slice(startIndex, (amount * pageNumber));
};
