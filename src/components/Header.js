import React, { Component } from 'react'
import '../styles/Header.sass'

class Header extends Component {

    render() {

        return (
            <header>
                <h1 className="logo">F-91W</h1>
            </header>
        )
    }
}

export default Header
