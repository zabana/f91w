import React, {Component} from 'react'
import '../styles/Footer.sass'

class Footer extends Component {

    render() {

        return (

            <footer>

                <div>

                    <p>
                        A website by  <a href="http://www.zabana.me" rel="noopener noreferrer" target="_blank">Zabana</a>
                    </p>

                    <ul className="social-links">
                       <li>
                           <a href="https://twitter.com/zabanaa_">Twitter</a>
                       </li>

                       <li>
                           <a href="https://github.com/zabanaa">Github</a>
                       </li>

                    </ul>
                </div>
            </footer>
        )
    }
}

export default Footer
