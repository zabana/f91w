import '../styles/PhotoList.sass';

import React, { Component } from 'react';

import Photo from './Photo';
import ErrorMessage from './Error';
import LoadMoreButton from './LoadMoreButtonConnector';

class PhotoList extends Component {

    componentWillMount() {
      this.props.fetchPhotos();
    }

    renderError(error) {
      return <ErrorMessage message={error.message} />
    }

    renderPhotoList(photos) {
      return photos.map((photo, i) => {
        return <Photo key={i} name={photo.name} image_url={photo.url} />
      });
    }

    render() {
      const {
        photosToDisplay,
        requestFailed,
        loadMorePhotosFailed,
        error
      } = this.props;
      return (
        <main>
          <section className="photoList">
            {
              requestFailed ?
              this.renderError(error):
              this.renderPhotoList(photosToDisplay)
            }
          </section>

          <section className="loadMoreSection">
            {
              loadMorePhotosFailed ?
              this.renderError(error) :
              <LoadMoreButton />
            }
          </section>

        </main>
      )
    }
}

export default PhotoList;
