import React, {Component} from 'react'
import '../styles/LoadMoreButton.sass'

class LoadMoreButton extends Component {

  render() {
    const {
      allPhotos,
      loadMorePhotos,
      currentPage,
      currentlyDisplayedPhotos
    } = this.props;
    return (
        <div className="load-more">
          <button onClick={() => {
            loadMorePhotos(currentPage, allPhotos, currentlyDisplayedPhotos)
          }}>
            Load More
          </button>
        </div>
    )
  }
}

export default LoadMoreButton
