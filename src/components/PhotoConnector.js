import { connect } from 'react-redux'
import { fetchPhotos } from '../actions/photosActions'
import PhotoList from './PhotoList'

const mapStateToProps = ({ photos }) => {
  return {
    photos
  }
};

const mapDispatchToProps = dispatch => {
  return {
    fetchPhotos: () => dispatch(fetchPhotos())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhotoList);
