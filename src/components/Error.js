import React, { Component } from 'react'

class ErrorMessage extends Component {
  render() {
    return(
      <section className="errorMessage">
        <h3>
          An error has occured. Please read the message below for more information.
        </h3>
        <p>{this.props.message}</p>
      </section>
    )
  }
}

export default ErrorMessage

