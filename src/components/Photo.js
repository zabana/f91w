import React, { Component } from 'react'

class Photo extends Component {
  render() {
    return(
      <figure>
        <img src={this.props.image_url} alt="{this.props.info.name}"/>
        <figcaption>{this.props.name}</figcaption>
      </figure>
  )
  }
}

export default Photo
