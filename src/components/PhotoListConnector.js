import { connect } from 'react-redux'
import { fetchPhotos } from '../actions/photosActions'
import PhotoList from './PhotoList'

const mapStateToProps = ({
  photosList: {
    photosToDisplay,
    requestFailed,
    loadMorePhotosFailed,
    error
  }
}) => {
  return {
    photosToDisplay,
    requestFailed,
    loadMorePhotosFailed,
    error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchPhotos: () => dispatch(fetchPhotos())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PhotoList);
