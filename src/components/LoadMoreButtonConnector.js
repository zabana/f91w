import { connect } from 'react-redux'
import { loadMorePhotos } from '../actions/photosActions'
import LoadMoreButton from './LoadMoreButton'

const mapStateToProps = ({
  photosList: {
    allPhotos,
    photosToDisplay,
    currentPage,
  }
}) => {
  return {
    currentlyDisplayedPhotos: photosToDisplay,
    allPhotos,
    currentPage,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadMorePhotos: (currentPage, allPhotos, currentlyDisplayedPhotos) => (
      dispatch(loadMorePhotos(currentPage, allPhotos, currentlyDisplayedPhotos))
    )
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoadMoreButton);
