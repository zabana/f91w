import {
  FETCH_PHOTOS_SUCCESS,
  FETCH_PHOTOS_ERROR,
  LOAD_MORE_PHOTOS_SUCCESS,
  LOAD_MORE_PHOTOS_ERROR,
} from '../actions/types';

import { getChunkOfPhotosToDisplay } from '../helpers/photoHelpers';

export const fetchPhotosSuccess = photos => {
  return {
    type: FETCH_PHOTOS_SUCCESS,
    payload: {
      allPhotos: photos,
      // initally show the first 10 photos
      photosToDisplay: getChunkOfPhotosToDisplay(photos, 0, 1, 10)
    }
  };
}

export const fetchPhotosError = error => {
  return {
    type: FETCH_PHOTOS_ERROR,
    payload: { error }
  };
}

export const loadMorePhotosSuccess = (photosToDisplay, newCurrentPage) => {
  return {
    type: LOAD_MORE_PHOTOS_SUCCESS,
    payload: {
      currentPage: newCurrentPage,
      photosToDisplay
    }
  }
}

export const loadMorePhotosError = error => {
  return {
    type: LOAD_MORE_PHOTOS_ERROR,
    payload: { error }
  }
}

