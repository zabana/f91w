import { map } from 'ramda';
import {
  createFileURL,
  getChunkOfPhotosToDisplay
} from '../helpers/photoHelpers';
import config from '../config';
import {
  fetchPhotosSuccess,
  fetchPhotosError,
  loadMorePhotosError,
  loadMorePhotosSuccess
} from './photosOperations';

export const fetchPhotos = () => async (dispatch) => {

  try {
    console.log("API URL", config.API_URL);
    const response = await fetch(`${config.API_URL}/photos`)
    const json = await response.json();
    const photos = map(createFileURL, json.body.photos);

    if (!response.ok) {
      const error = { status: response.status, message: response.statusText }
      return dispatch(fetchPhotosError(error));
    };

    return dispatch(fetchPhotosSuccess(photos));

  } catch (err) {
    return dispatch(fetchPhotosError(err));
  }

}

export const loadMorePhotos = (currentPage, allPhotos, currentDisplayedPhotos) => dispatch => {

  // 0. Increment currentPage number
  const newCurrentPage = currentPage + 1;
  // 1. copy the array of currently displayed photos
  const photosToDisplay = currentDisplayedPhotos.slice(0)
  // 2. get a new chunk of photos to display (by slicing photosToDisplay)
  const chunkStartIndex = photosToDisplay.length;
  const photosToAdd = getChunkOfPhotosToDisplay(
    allPhotos, chunkStartIndex, newCurrentPage, 10
  );
  // 3. for each, push them to the newArray
  photosToAdd.forEach( photo => photosToDisplay.push(photo))
  // 4. return the new Array in the payload

  if (photosToAdd.length === 0) {
    const error = { status: null, message: "You've reached the end of the list" }
    return dispatch(loadMorePhotosError(error))
  }

  return dispatch(loadMorePhotosSuccess(photosToDisplay, newCurrentPage))
}
