FROM node:carbon-alpine as builder

COPY . /app

WORKDIR /app

RUN npm install

RUN npm run build

EXPOSE 5000
